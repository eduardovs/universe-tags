# Universe Tags
Contributors: Eduardo Vilso

Blog link: eduardovilso.online/blog

Tags: multi tags, etiqueta multiple html, vanilla js tags input

License: GPLv2 or later

License URI: https://www.gnu.org/licenses/gpl-2.0.html

## == Description == ##

universe tags is a simple widget that help us to create a label dynamic multiple and save the data in an array.

## == How to use == ##
Include "universe-tags.js" in your file as example "index.html".
I would recommend insert  in your footer this code.
```
<script type="text/javascript" src="universe-tags.js">
<script type="text/javascript">
    universeTags.init()
</script>
```

## == Support & suggestions == ##
hola@eduardovilso.online

## Thanks ##
