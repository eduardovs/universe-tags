const universeTags = function () {
    var elements
    var tags = []
    
    
    function init() {
        element = document.querySelector('.universe-tags input')
        element.addEventListener("keyup", pressKey)
        element.addEventListener("focus", focusBox)
        element.addEventListener("focusout", focusBox)
        
    }

    function pressKey(e) {
        let character = e.which
        let current = e.currentTarget

        if (character == 188) {
            let word = cleanString(current.value.slice(0, -1))
            createTag(current, word)
        }
    }

    function focusBox(e) {
        let current = e.currentTarget
        let value = cleanString(current.value)
        let words  = value.split(" ");
        
        for(word of words){
            if (word !== "") {
                createTag(current, word)
            }
        }
    }

    function createTag(current, word) {
        tags.push(word)
        let item = document.createElement("label")
        item.classList.add('item')
        item.addEventListener("click", deleteTag)
        let label = document.createTextNode(word)
        item.appendChild(label)
        current.parentNode.appendChild(item)
        current.value = ''
    }

    function deleteTag(e) {
        let element = e.currentTarget;
        let word    = element.innerText;
        tags        = tags.filter(item => item !== word)
        element.remove();
    }

    function cleanString(value) {
        return value.replace(/[&\/\\#,_+()$~%.'":*?<>{}]/g, '')
    }
    
    /***
    * Method return all tags of array
    * @return array
    ***/
    function allTags() {
        return tags
    }

    return {
        init,
        allTags
    }
}()